FROM ruby:2.6.3

ENV RUBYLIB /opt/kropotkin/
WORKDIR $RUBYLIB

COPY Gemfile* $RUBYLIB
RUN bundle install

COPY . $RUBYLIB

CMD ["bundle", "exec", "ruby", "up.rb"]
