init:
	docker-compose up --build

lock:
	docker-compose run --rm kropotkin bundle lock

build:
	docker-compose build
